#include<mpi.h>
#include<iostream>
#include<cstring>

 
//Method to calculate the 'DeterminantMod26
int calculateInverDeterminant(int a, int b, int c, int d){


    int det = ((a*d) - (b* c)) %26;

    //If value is negative we add 26
    if(det < 0)
        det += 26;

    //We create a standard int array, and look for our 'int det', that we just calculated    
    int arr[] =        {1,3,5,7,9,11,15,17,19,21,23,25};

    //We find the corresponding index in 'inverseArr' and take the value from it. This will be our 'return'
    int inverseArr[] = {1,9,21,15,3,19,7,23,11,5,17,25};

    //find the index of 'det' in arr
    int index = -1;

    //We loop through 'arr' until we find 'det'
    for(int i = 0; i < 12; i++){

        if (arr[i] == det){  
        index = i;
        break;
     }
    }

    //We take the value from the index in 'InverseArr' that corresponds to the index value of 'det' in 'arr'
    int invDet = inverseArr[index];

    //returns the 'Determinant' 
    return invDet;

}

 //int* calculateInverse(int a, int b, int c, int d){
 int* calculateInverse(int a, int b, int c, int d){


    
   

    //We use return from 'calculateInverDeterminent to get our 'Determinant
    int alpha = calculateInverDeterminant( a,  b,  c,  d);

    std::cout<<"1/DeterminentMod26 is "<< alpha<< std::endl;
    std::cout<<std::endl;


    //If determinant == 0, we must return 'nullptr', as it cannot be used.
    if(alpha == 0){
        return nullptr;
    }

    //We find the  (Determinant*Adjoint). This is the Formula for INVERSE

    //Create new array to store value of (Determinant*Adjoint).
    int* inverse = new int[4];


         inverse[0] = (alpha * d) % 26;
         inverse[1] = (alpha * -b) % 26;
         inverse[2] = (alpha * -c) % 26;
         inverse[3] = (alpha * a) % 26;

         for(int i = 0; i < 4; i++){

            if(inverse[i] < 0){
                inverse[i] += 26;
            }
         }

      

        return inverse;
 }

 

   

 //Method to check if 'Inverse Matrix' has been calculated correctly
 void checkIdenty(int a, int b, int c, int d){

        //Array is created to store values of original 'key matrix' (user input)
           int* original = new int[4];
                original[0] = a;
                original[1] = b;
                original[2] = c;
                original[3] = d;



          //Creating a new array to store results of 'calculateInverse'
           int* copy = new int[4];
           

                copy = calculateInverse(a,b,c,d);

            //Creating a new array to store results of multiplicating 'Inverse' by Origional matrix
            int* identy = new int[4];

                identy[0] = ((original[0]* copy[0]) + (original[1] * copy[2]))%26;
                identy[1] = ((original[0]* copy[1]) + (original[1] * copy[3]))%26;
                identy[2] = ((original[2]* copy[0]) + (original[3] * copy[2]))%26;
                identy[3] = ((original[2]* copy[1]) + (original[3]* copy[3]))%26;

                for(int i = 0; i < 4; i++){

            //If values are negative we add 26
            if(identy[i] < 0){
                identy[i] += 26;
            

             }
                                    //Printing to console values of Identy matrix
                                     std::cout<<" Here are the elements of array Identy "<<identy[i]<<std::endl;

            }


                
 }

//Method to find the integer value of decoded text
 int* decodeBlock(int* part, int* key){

    
    


    int* result = new int[2];

   // part = new int[2];

               result[0] = ((key[0]*part[0]) + key[1]*part[1])%26;
               result[1] = ((key[2]*part[0]) + key[3]*part[1])%26;



    return result;

 }





//Method to print contents of char array to console
 void printArray(char* arrayw,int sizew){

    //Statement to consol calling attantion to the decrypted text (now Plaintext)
    std::cout<<"Below is the DECRYPTED text, printed Horizontally: "<< std::endl;
    std::cout<<std::endl;

    for(int i = 0; i < sizew; i++){
        //Printing contents of char array
        std::cout<<arrayw[i];
    }
    std::cout<<std::endl;

}

//Array to store index number of characters in decrypted text
int arrInt[12];

//Variable to store rank number
int rank;

//Variable to count number of processes in communicator
int size;




int main(int argc, char **argv) {

    //Initalizing MPI_Init
    MPI_Init(&argc, &argv);

    //Initalizing MPI_Comm_rank, which keeps track of rank(s) running in the communicator
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //Initalizing MPI_Comm_size, which keeps track of number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &size);

                   if (size != 6) {
        if (rank == 0) {
            std::cout << "WARNING: This program only runs with 6 processes" << std::endl;
        }
        MPI_Finalize();
        return 1;
    }

    if (rank == 0) {
        std::cout << "Student name: Rory O Donoghue" << std::endl;
        std::cout << "Student number: -------" << std::endl;
    }
   

    //task (a)

        //Creating an Pointer to an array(will be used to store result from  'calculateInverse')
         int* arr1;

        //Creating a matrix for user input
         int matrix [2] [2];

        //Using Coordinator to collect user input
         if(rank == 0){

             //Requesting user to enter Integers for our matrix
             std::cout<<"Please Enter the following four Integers as Your Matrix Key: 3,6,4,5 "<<std::endl;
             //Using for loop to collect user input and store in 'matrix'
             for(int i = 0; i < 2; i++){
                 for(int j= 0; j < 2; j++){
                 //Assiging input to 'matrix' using 'cin' operator    
                 std::cin>> matrix[i][j];
            }

          }

   

    


    //task(b)

             arr1 =  calculateInverse(matrix[0][0],matrix[0][1],matrix[1][0],matrix[1][1]);
    //Task(c)  
            for(int i = 0; i < 4; i++){    
                 std::cout<<"here are the elements of the inverse Matrix "<<arr1[i]<<std::endl;

           }
                 std::cout<<std::endl;
        }//End of if loop

    //task(d)

    //Using Process 0 to call  MPI_Bcast() and send each process in communicator the 'key matrix'
    MPI_Bcast(arr1,4,MPI_INT,0,MPI_COMM_WORLD);


     //task(e)  
    
         //Using rank 0 (the coordinator) to accept user input
         if(rank == 0){   

            //Creating an a 'char' array of length 13(12 places for text an 1 for null character)
            char input[13] ;

            //Promting User to enter text
            std::cout<<"Please Enter the Following Text: Dfanoryhmduy "<<std::endl;

            //assiging text to created array
            std::cin>>input;
            std::cout<<std::endl;

            //Ensuring all entered text is uppercase (will be  automatically converted by following code)
            for(int i = 0; i < 12; i++){

               //To uppercase 
               input[i] = toupper(input[i]);

            }
         
    
            //Creating an int for use in for loop
            int store = 12;

            //Two Arrays: serve as a Conversion Table for Characters and Integers
             int numbers  [] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
             char letters [] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};



       
       
            //Using a for loop to convert the input text from the user to integers
            for(int i = 0; i < store ;i++){

      
                    for(int j = 0; j <  26 ;j++){

                     //Finding the place in the letter table of the character(s) the user entered
                     if(input[i] == letters[j]){
                         //Marking the number of the entered character in an int array
                        arrInt[i] = numbers[j];
                            

                     }

                 }
             }     

                std::cout<<std::endl;
             
            //Printing to console the index number of the entered character(s)
            for(int i = 0; i < 12; i++){ 

                  //an array named 'arrInt' holds the index numbers of entered characters
                  std::cout<<"here are the elements of arrInt "<< arrInt[i]<<std::endl;

                 }


         }//if of For loop

            //int 'chunk_size' will vary depending on number of processes
            int chunk_size = 12/size;
            //size of 'recv' array will vary depending on number of processes

            int* recv = new int[chunk_size];

    //task(f)

          //The 'Coordinator' (rank 0) will scatter to the participants (all ranks) including itself a 'chunk' of array        
    MPI_Scatter(arrInt,chunk_size,MPI_INT,recv,chunk_size,MPI_INT,0,MPI_COMM_WORLD);


 
    
    //task(g)
        
            //Creating a new array to use in MPI_Gather. size will depend on number of processes.
            int* total = new int[chunk_size];

            //Creating a new int array to gather all the 
            int* gather = new int[chunk_size];
                   //Putting the results of each process into array 'gather'
                    gather = decodeBlock(recv,arr1); 

    //task(h)                 

         //The Coordinator uses MPI_Gather to gather the decodedBlock() results from each Process
    MPI_Gather(gather,chunk_size,MPI_INT,total,chunk_size,MPI_INT,0,MPI_COMM_WORLD);

                 
            //Again two Arrays are used to serve as a 'Conversion Table'. This Time from 'Number' to 'Letter'
             int numbers  [] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
             char letters [] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            //Creating achar array to store the Plaintext(decrypted text)
            char* last = new char[13];

            //using for loop to loop through both 'Numbers' and 'Letters' inorder to find index of character in' Letters'
            if(rank == 0){
                  for(int i = 0; i < 12; i++){
                for(int j = 0; j < 26; j++)

                if(total[i] == numbers[j]){
                    //Plaintext will be stored in array 'last'
                    last[i] = letters[j];
                }
            }



            }

    //task(i)
            //Again using the 'Coordinator' (rank 0) 
            if(rank == 0){ 
                            //Coordinator uses method 'printArray' to print contents of char array 'last' to consol
                            printArray(last,12);

            }
            std::cout<<std::endl;
       
                  if(rank == 0){ 
                            //Coordinator uses 'checkIdenty() method to verify if Inverse matrix was calculated correctly
                            checkIdenty(3,6,4,5);

            }
       

          

    //Finishing all our MPI work    
    MPI_Finalize();

        //Will return 0 if no errors in program
        return 0;

}

