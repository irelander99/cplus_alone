
# C++ Program with MPI Interface

**Please Note:** It is necessary to have an MPI Library installed on your system in order to run this program.

There are a number of MPI Libraries available. For this program, I am using 'Open MPI'. You can refer to YouTube videos for guidance on how to download and install the MPI Library.

Additionally, as part of a college assignment, I have written a report on this program. You can find the report under the 'Distributed Systems' section on the Portfolio Page.





